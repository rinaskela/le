module MyModule
    function my_sum(x::T, y::T)::T where {T}
        return (x + y)
    end
    
    export my_sum
end # end of MyModule

MyModule.my_sum(2, 3) |> println

function MyModule.my_sum(x::Float64, y::Float64)
    return "ho-ho"
end

MyModule.my_sum(2.0, 3.0) |> println

# Base.:(+)(x::Int64, y::Int64) = "ho-ho"