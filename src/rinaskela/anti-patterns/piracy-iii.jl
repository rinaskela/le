struct Party
    title::String
    guests::Vector{String}
end

Party(title) = Party(title, String[])

Base.join(name::String, party::Party) = push!(party.guests, name)

p = Party("le-6")

join("Саша Ю", p) |> println
join("Саша К", p) |> println

join(["Саша К", "Саша Н"], p) |> println