function decor(func::Function)
    function wrap()
        println("-"^60)
        func
        println("-"^60)
    end

    return wrap()
end

function my_func(a)
    println(a)
end

my_func_decorated = decor(my_func)

my_func = decor(my_func)