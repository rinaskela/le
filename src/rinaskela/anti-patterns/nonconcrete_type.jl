using Statistics: mean
using BenchmarkTools

struct Point{T <: Real}
    x::T
    y::T
end

struct PointAny
    x
    y
end

function center(points::AbstractVector{T}) where T
    return T(
        mean(p.x for p in points),
        mean(p.y for p in points)
    )    
end

make_points(T::Type, n) = [T(rand(), rand()) for _ in 1:n]

points = make_points(PointAny, 100_00)

@btime center($points)

points = make_points(Point, 100_00)

@btime center($points)