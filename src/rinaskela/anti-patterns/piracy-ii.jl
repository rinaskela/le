module MyModule
    import Base.+

    (+)(s::AbstractString, n::Number) = "$s$n"
    
    # "1" + 2 -> "12"
end # end of MyModule

module AnotherModule
    import Base.+

    (+)(s::AbstractString, n::T) where T <: Number = parse(T, s) + n
    
    # "1" + 2 -> 3
end # end of AnotherModule

# Сломается при неломающих изменениях

module RightModule
    export str_str
    import Base.+

    struct MyString
        val::String
    end

    macro str_str(s::AbstractString)
        MyString(s)
    end

    (+)(s::MyString, n::Number) = "$s$n"
    
    # "1" + 2 -> "12"
end # end of RightModule