function sp(a::Vector{Float64}, b::Vector{Float64})
    return sum(a .* b)
end # не работает с остальными типами

function sp(a::Vector{Number}, b::Vector{Number})
    return sum(a .* b)
end # Vector{Float64} <: Vector{Number} -> false

function sp(a::Vector{T}, b::Vector{T}) where T <: Number
    return sum(a .* b)
end # нельзя разные

function sp(a::Vector{S}, b::Vector{T}) where {S <: Number, T <: Number}
    return sum(a .* b)
end # только вектора

function sp(a::Array{S, N}, b::Array{T, N}) where {N, S <: Number, T <: Number}
    return sum(a .* b)
end # нельзя разреженные матрицы

function sp_6(a::AbstractArray{S, N}, b::AbstractArray{T, N}) where {N, S <: Number, T <: Number}
    return sum(a .* b)
end

function sp_7(a, b)
    return sum(a .* b)
end

using SparseArrays
A = sparse([1, 10, 100], [1, 10, 100], [1, 2, 3])
# println(A) -> terminal

using BenchmarkTools

a = rand(10_00)
b = rand(10_00)

println("\$ +")
@btime sp_6($a, $b)
@btime sp_7($a, $b)

println("\$ -")
@btime sp_6(a, b)
@btime sp_7(a, b)