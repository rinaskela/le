struct SomeName2{T <: Number, S <: Number}
    a::T
    b::S

    function SomeName2(a::T, b::S) where {T, S}
        new{T, S}(a::T, b::S)
    end
end

function some_function(sn1::SomeName2{T, S}) where {T,S}

end

function some_function(sn1::SomeName2{UInt8, UInt8})

end



struct SomeName{T <: ParentOne}
    x::T
    y::T

    SomeName(a::T) where {T} = new{T}(a, a)
end

function some_function(sn::SomeName{T}) where {T}
    
end


function some_function(sn::SomeName{ChildTwo{String}}) where {T}
    
end