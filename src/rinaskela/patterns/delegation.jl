struct ToyHouse
    child::Parent
    parts::Int64

    ToyHouse(x, parts) = new(
        ChildOne(x::Union{Int64, Float64}),
        parts
    )
end

interface_show(th::ToyHouse) = interface_show(th.child)