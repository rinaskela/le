module Patterns

using Macro

include("delegation.jl")
include("holy_traits.jl")
include("parametric_type.jl")
    
end