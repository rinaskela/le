abstract type IsSweetTooth end

struct SweetTooth <: IsSweetTooth end
struct NotSweetTooth <: IsSweetTooth end

IsSweetTooth(::Type) == SweetTooth()
IsSweetTooth(::Type{<:ParentOne}) == NotSweetTooth()
IsSweetTooth(::Type{ChildThree}) == NotSweetTooth()

want_some_candy(child::T) where {T} = want_some_candy(IsSweetTooth(T), child)
want_some_candy(::NotSweetTooth, child) = false
want_some_candy(::SweetTooth, child) = true