using DifferentialEquations

# f(u, p, t) = 1.01 * u

function parameterized_lorenz!(du, u, p, t)
     du[1] = p[1] * (u[2] - u[1])
     du[2] = u[1] * (p[2] - u[3]) - u[2]
     du[3] = u[1] * u[2] - p[3] * u[3]
end

# u0 = 1 / 2
# tspan = (0.0, 1.0)
# prob = ODEProblem(f, u0, tspan)

u0 = [1.0, 0.0, 0.0]
tspan = (0.0, 100.0)
p = [10.0, 28.0, 8 / 3]
prob = ODEProblem(parameterized_lorenz!, u0, tspan, p)

sol = solve(prob, Tsit5(), reltol = 1e-8, abstol = 1e-8)

using Plots
# plot(sol, dpi = 320, linewidth = 5, title = "Solution to the linear ODE with a thick line",
#      xaxis = "Time (t)", yaxis = "u(t) (in μm)", label = "My Thick Line!") # legend=false
# plot!(sol.t, t -> 0.5 * exp(1.01t), lw = 3, ls = :dash, label = "True Solution!")

plot(sol, dpi = 320, idxs = (1, 2, 3))

savefig("diff_eq_1.png")