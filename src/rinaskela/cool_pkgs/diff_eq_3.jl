using DifferentialEquations

function f(du, u, p, t)
    du[1] = -u[1]
end

u0 = [10.0]
const V = 1

prob = ODEProblem(f, u0, (0.0, 10.0))

# sol = solve(prob, Tsit5())

condition(u, t, integrator) = t == 4
affect!(integrator) = integrator.u[1] += 10
cb = DiscreteCallback(condition, affect!)
# sol = solve(prob, Tsit5(), callback = cb)
sol = solve(prob, Tsit5(), callback = cb, tstops = [4.0])

using Plots;
plot(sol, dpi = 320);

savefig("diff_eq_3.png")