module Rinaskela

export Intro, JuliaStandardPkg

include("intro/intro.jl")
include("julia_standard_pkg/julia_standard_pkg.jl")
include("dispatch/dispatch.jl")
include("macro/macro.jl")
include("patterns/patterns.jl")

end
