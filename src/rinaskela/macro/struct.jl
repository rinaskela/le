struct ChildOne <: ParentOne
    int_field::Int64
    float_field::Float64

    function ChildOne(x::Union{Int64, Float64})
        int_field = x isa Int64 ? 1 : 0
        float_field = x isa Float64 ? 1 : 0
        new(int_field, float_field)
    end
end


struct ChildTwo{T} <: ParentOne
    int_field::Int64
    float_field::Float64

    function ChildTwo(x::T) where {T}
        int_field = T == Int64 ? 1 : 0
        float_field = T == Float64 ? 1 : 0
        new{T}(int_field, float_field)
    end
end

function interface_show(s::ChildTwo{Int64})
    println("Child Two: ", s)
end

function interface_show(s::ChildTwo{Float64})
    println("Child Two: ", s)
end

function interface_show_type(t::Type{ChildTwo{T}}) where T # а если убрать T?
    println("Type of Child Two: ", t)
end


struct ChildThree{T} <: ParentTwo
    int_field::Int64
    float_field::Float64

    function ChildThree(x::T) where {T}
        int_field = T == Int64 ? 1 : 0
        float_field = T == Float64 ? 1 : 0
        new{T}(int_field, float_field)
    end
end

@interfaces("Some string", "ChildThree")
