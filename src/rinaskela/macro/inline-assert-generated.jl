@assert 1 == 1 "One!"

@generated function bar(x)
    if x <: Integer
        return :(x ^ 2)
    else
        return :(x)
    end
end

3 |> bar |> println
3.0 |> bar |> println

f(x) = x^x
g(x) = for _ in 1:x f(x) * x end

@inline foo(x) = x^20
@inline goo(x) = for _ in 1:x foo(x) * x end

@time g(100000)
@time goo(100000)