abstract type Parent end
abstract type ParentOne <: Parent end
abstract type ParentTwo <: Parent end

function interface_show(s::Parent)
    error("interface_show must be implemented for $s")
end

function interface_show_type(::Type{Parent})
    error("interface_show_type must be implemented for $s")
end

function interface_show(s::ParentOne)
    println("Parent One: ", s)
end

function interface_show_type(t::Type{<:ParentOne})
    println("Type of Parent One: ", t)
end