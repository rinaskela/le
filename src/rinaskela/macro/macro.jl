module Macro

export Parent, ParentOne, ParentTwo

export ChildOne, ChildTwo, ChildThree

export @interfaces

include("macros.jl")
include("interfaces.jl")
include("struct.jl")
include("run.jl")

end