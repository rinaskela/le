macro interfaces(output_string::String, struct_name::String)
    string_to_return = [
        "function interface_show(s::$struct_name)",
        "    println(\"$output_string and $struct_name: \", s)",
        "end",
        "",
        "function interface_show_type(s::Type{$struct_name{T}}) where T",
        "    println(\"$output_string and type of $struct_name: \", s)",
        "end",
    ]

    # for string in string_to_return
    #     string |> println
    # end

    return esc(Meta.parseall(join(string_to_return, "\n")))
end