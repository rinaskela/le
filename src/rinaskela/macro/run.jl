cashe = [ChildOne, ChildTwo, ChildTwo, ChildThree]
struct_cashe = Vector{Parent}()


for my_struct in cashe
    push!(struct_cashe, my_struct(1))
end

for my_struct in struct_cashe
    interface_show(my_struct)
end

println("-"^60)

for my_struct in struct_cashe
    interface_show_type(typeof(my_struct))
end