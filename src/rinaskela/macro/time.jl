@generated function bar(x)
    if x <: Integer
        return :(x ^ 2)
    else
        return :(x)
    end
end


function car{Int64}(x)
    return x ^ 2
end

function car{Float64}(x)
    return x
end

g(x) = for _ in 1:x car{Int64}(3) end
t(x) = for _ in 1:x bar(3) end

@time g(200000)
@time t(200000)