include("test_alloc.jl")

using Profile
#using PProf

myfunc()

@profile myfunc()

Profile.print()

# pprof()

Profile.clear()