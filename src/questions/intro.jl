"""
Место для Ваших вопросов:

Создание пустых матриц заданной размерности:
Array{Float64}(undef, 0, 0)

Как под капотом работает макрос @generated:
https://github.com/JuliaLang/julia/blob/17cfb8e65ead377bf1b4598d8a9869144142c84e/base/expr.jl#L807-L835
"""