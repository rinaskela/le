tests:
	julia --code-coverage=user --project -e "import Pkg; Pkg.test()"

delete_cov:
	find . -name "*.cov" -type f -delete

le-1:
	git checkout 1d8c5b0deefeff67162640257547fa98d5940bf8

le-2:
	git checkout ff14676aba608c4e0d992d374f20fa0e5c1875c5

le-3:
	git checkout 383655614033afa000966f2e71064e70c50261d3

le-4:
	git checkout c76a90d882c44d231a9e9db7d689bb67391e6c2c

le-5:
	git checkout 4e5bf766ff9e0732bd0180c81a7cae2019cfbe1e

le-6:
	git checkout c2b217ab98c8046b3bf46a441e576c3f635d700b