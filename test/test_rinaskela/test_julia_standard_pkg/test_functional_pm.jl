import le.Rinaskela.JuliaStandardPkg.Money
import le.Rinaskela.JuliaStandardPkg.accumulate_money
import le.Rinaskela.JuliaStandardPkg.correct_money
import le.Rinaskela.JuliaStandardPkg.staff_count

@testset "Functional PM" begin
    money_input = Money(100)
    @testset "Correct money with right argument" begin
        money_output = Money(50)
        @test correct_money(money_input, 0.5) == money_output
    end

    @testset "Correct money with wrong argument" begin
        expected_message = "Percent must be positive"
        @test_throws ArgumentError(expected_message) begin
            correct_money(money_input, -0.5)
        end
    end

    @testset "Accumulate money with right argument" begin
        money_output = Money(50)
        @test accumulate_money(money_input, -50) == money_output
    end

    @testset "Staff count with right argument" begin
        expexted_number = 393
        @test staff_count(50) == expexted_number
    end
end
